exports.config = {
	namespace: 'dg-portfolio',
	baseUrl: '/projects/portfolio/',
	outputTargets:[
		{
			type: 'dist',
			serviceWorker: false
		},
		{
			type: 'www',
			serviceWorker: false
		}
	],
	globalStyle: []
};

exports.devServer = {
	root: 'www',
	watchGlob: '**/**'
}
