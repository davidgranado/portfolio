import { Component, Event, Prop, EventEmitter } from '@stencil/core';
import classnames from 'classnames';

@Component({
	tag: 'dg-tabs',
	styleUrl: 'dg-tabs.css',
})
export class DgPortfolio {
	@Prop() selectedTabIndex = 0;
	@Prop() tabs: string[];
	@Event() selectTab: EventEmitter;
	render() {
		return (
			<ul class="tab tab-block">
				{this.tabs.map((tab, i) => (
				<li
					class={
						classnames('tab-item btn btn-link', {
						active: this.selectedTabIndex === i,
					})}
					onClick={() => this.handleTabSelected(i)}
				>
					{tab}
				</li>
				))}
			</ul>
		);
	}

	private handleTabSelected(tabIndex: number) {
		this.selectTab.emit(tabIndex);
	}
}
