import { Component, Prop } from '@stencil/core';

@Component({
	tag: 'dg-nav',
	styleUrl: 'dg-nav.css'
})
export class DgNav {

	@Prop() name: string;
	@Prop() avatarUrl: string;

	render() {
		return (
			<header class="navbar">
				<section class="navbar-section">
				</section>
				<section class="navbar-center">
					<h1>
						{this.name}
					</h1>
				</section>
				<section class="navbar-section">

				</section>
			</header>
		);
	}
}
