import { Component, Prop } from '@stencil/core';

@Component({
	tag: 'dg-avatar',
})
export class DgAvatar {

	@Prop() avatarUrl: string;
	@Prop() altText: string;

	render() {
		return (
			<figure class="avatar avatar-xl">
				<img src={this.avatarUrl} alt={this.altText} />
			</figure>
		);
	}
}
