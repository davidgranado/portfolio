import { Component, Event, EventEmitter, Prop } from '@stencil/core';

import { ProjectLabel } from '../../models';


@Component({
	tag: 'dg-label-filter',
	styleUrl: 'dg-label-filter.css'
})
export class DgLabelFilter {
	@Prop() labelList: ProjectLabel[];
	@Prop() selectedLabels: ProjectLabel[];
	@Event() labelClicked: EventEmitter;

	render() {
		return (
			<div class="faded-out fade-in">
				{this.labelList.map(label => (
					<dg-project-label
						class="c-hand"
						type={label}
						isInactive={!this.labelIsSelected(label)}
						onClick={() => this.handleClickLabel(label)}
					/>
				))}
			</div>
		);
	}

	private handleClickLabel(label: ProjectLabel) {
		this.labelClicked.emit(label);
	}

	private labelIsSelected(label: ProjectLabel) {
		return this.selectedLabels.indexOf(label) !== -1;
	}
}
