import { Component, Prop, State } from '@stencil/core';
import classnames from 'classnames';

import { Project, ProjectLabel } from '../../models';

@Component({
	tag: 'dg-projects',
	styleUrl: 'dg-projects.css',
})
export class DgProjects {
	@Prop() projects: Project[];
	@Prop() name: string;
	@Prop() avatarUrl: string;
	@Prop() onOpenProject: (heading: string, content: JSX.Element) => void;
	@State() openProject: Project | null = null;
	@State() selectedProjectLabels: ProjectLabel[];

	private modalEl: HTMLDgProjectDetailElement | null = null;

	componentWillLoad() {
		this.selectedProjectLabels = ['business'];
	}

	closeModal() {
		if(this.modalEl) {
			this.modalEl.remove();
			this.modalEl = null;
		}
	}

	openProjectModal(project: Project) {
		if(this.modalEl) return;

		this.modalEl = document.createElement('dg-project-detail');
		this.modalEl.project = project;
		this.modalEl.addEventListener('close', () => this.closeModal());

		document.body.appendChild(this.modalEl);
	}

	get allProjectLabels() {
		if(!this.projects) {
			return [];
		}

		const labelList = this.projects
			.reduce(
				(combinedList, project) =>
					combinedList.concat(project.labels)
				, []
			);
		const labelMap = labelList.reduce(
				(labelObj, label) =>
					labelObj[label] = true && labelObj,
				{}
			);
		const uniqueLabels = Object.keys(labelMap);
		return uniqueLabels.sort() as ProjectLabel[];

	}

	render() {
		return (
			<div>
				<p class="subtitle">
					<div class="reveal">Software Engineer</div>
					<div class="reveal">UI Specialist</div>
					<div class="reveal cross-out">
						UX Designer
						<div/>
					</div>
				</p>
				<dg-label-filter
					labelList={this.allProjectLabels}
					selectedLabels={this.selectedProjectLabels}
					onLabelClicked={(ev) => this.toggleLabelSelection(ev.detail)}
				/>
				<div class="columns">
					{this.projects.map((project, i) => (
						<dg-project-card
							imageUrls={project.imageUrls}
							mainTitle={project.title}
							subTitle={project.subTitle}
							labels={project.labels}
							index={i}
							onClick={() => this.openProjectModal(project)}
							class={classnames('column col-4 col-md-6 col-sm-12 shrinkable', {
									shrunken: !this.projectIsSelected(project),
							})}
						/>
					))}
				</div>
			</div>
		);
	}

	private projectIsSelected(project: Project) {
		if(!this.selectedProjectLabels.length) {
			return true;
		}
		return !!project.labels.find((label) => this.selectedProjectLabels.indexOf(label) !== -1);
	}

	private toggleLabelSelection(label: ProjectLabel) {
		if(this.labelIsSelected(label)) {
			this.selectedProjectLabels = this.selectedProjectLabels
				.filter(selectedLabel => selectedLabel !== label);
		} else {
			this.selectedProjectLabels = [
				label,
				...this.selectedProjectLabels
			];
		}
	}

	private labelIsSelected(label: ProjectLabel) {
		return this.selectedProjectLabels.indexOf(label) !== -1;
	}
}
