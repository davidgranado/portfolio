import { Component, Prop } from '@stencil/core';

const baseOffset = 5000;

@Component({
	tag: 'dg-project-card',
	styleUrl: 'dg-project-card.css',
})
export class DgProjectCard {

	@Prop() mainTitle: string;
	@Prop() subTitle: string;
	@Prop() imageUrls: string[];
	@Prop() index = 0;
	@Prop() labels: string[];

	render() {
		return (
			<dg-card-reveal
				delay={baseOffset + (this.index + 1) * 200}
				staggerRange={0}
				class="parallax c-hand"
			>
				<div class="parallax-top-left" tabindex="1"></div>
				<div class="parallax-top-right" tabindex="2"></div>
				<div class="parallax-bottom-left" tabindex="3"></div>
				<div class="parallax-bottom-right" tabindex="4"></div>
				<div class="parallax-content">
					<div class="parallax-front">
						<h2>{this.mainTitle}</h2>
					</div>
					<div class="parallax-back">
						<div class="card-wrapper">
							<div class="card">
								<div class="card-header">
									{this.subTitle}
								</div>
								<div class="card-image">
									<dg-image-rotator
										imageUrls={this.imageUrls}
										timingOffset={baseOffset + (this.index % 4) * 2000}
									/>
								</div>
								<div class="card-body">
									<div>
										{this.labels && this.labels.map((label) => (
											<dg-project-label
												type={label}
											/>
										))}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</dg-card-reveal>
		);
	}
}
