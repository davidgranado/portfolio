import { Component, Prop, Event, EventEmitter } from '@stencil/core';

import { Project } from '../../../../models';

@Component({
	styleUrl: 'dg-project-detail.css',
	tag: 'dg-project-detail',
})
export class DgProjectDetail {
	@Prop() project: Project | null = null;
	@Event() close: EventEmitter;

	handleClose() {
		this.close.emit();
	}

	render() {
		return (
			<div class={`modal ${this.project ? 'active' : ''}`}>
				<div
					onClick={() => this.handleClose()}
					class="modal-overlay"
				></div>
				{this.project && (
					<div class="modal-container">
						<div class="modal-header">
							<button
								class="btn btn-clear float-right"
								onClick={() => this.handleClose()}
							></button>
							<div class="modal-title h4">
								{this.project.title}
							</div>
						</div>
						<div class="modal-body">
							<div class="content">
								<dg-image-rotator
									imageUrls={this.project.imageUrls}
								/>
								<ul>
									{this.project.links.map((link) => (
										<li>
											{link.url ? (
												<a href={link.url} target="_blank">
													{link.label}
												</a>
											): (
												link.label
											)}
										</li>
									))}
								</ul>
								{this.project.lessons && (
									<span>
										<h4>Lessons Learned</h4>
										<p>
											{this.project.lessons}
										</p>
									</span>
								)}
								{this.project.technologies && (
									<span>
										<h4>Technologies Used</h4>
										<ul>
											{this.project.technologies.map(t => (
												<li>
													{t}
												</li>
											))}
										</ul>
									</span>
								)}
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" onClick={() => this.handleClose()}>
								Close
							</button>
						</div>
					</div>
				)}
			</div>
		);
	}
}
