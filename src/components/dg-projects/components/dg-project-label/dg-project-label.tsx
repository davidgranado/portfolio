import { Component, Prop } from '@stencil/core';

import { ProjectLabel } from '../../../../models';

export
const ProjectLabels: {
	[type: string]: ProjectLabel,
} = {
	hiatus: 'hiatus',
	hobby: 'hobby',
	business: 'business',
	game: 'game',
	'in-dev': 'in-dev',
	inactive: 'inactive',
	active: 'active',
};

const Labels = {
	[ProjectLabels.active]: 'A',
	[ProjectLabels.business]: 'B',
	[ProjectLabels.game]: 'G',
	[ProjectLabels.hobby]: 'H',
	[ProjectLabels.hiatus]: 'H',
	[ProjectLabels['in-dev']]: 'I',
	[ProjectLabels.inactive]: 'I',
};

const Colors = {
	[ProjectLabels.active]: '#00ffff',
	[ProjectLabels.business]: '#00a200',
	[ProjectLabels.game]: '#6600ff',
	[ProjectLabels.hobby]: '#0000ff',
	[ProjectLabels.hiatus]: '#d2bc04',
	[ProjectLabels['in-dev']]: '#67d0c2',
	[ProjectLabels.inactive]: '#ff0000',
};

@Component({
	tag: 'dg-project-label',
})
export class DgProjectLabel {
	@Prop() type: string;
	@Prop() isInactive = false;

	render() {
		return (
			<div class={`chip ${this.isInactive ? 'is-inactive' : ''}`}>
				<figure
					data-initial={Labels[this.type]}
					class={`avatar avatar-sm bg-secondary ${this.type}`}
					style={{
						backgroundColor: Colors[this.type],
					}}
				></figure>
				{this.type}
			</div>
		);
	}
}
