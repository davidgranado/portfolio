import { Component, Prop, State } from '@stencil/core';

@Component({
	styleUrl: 'dg-card-reveal.css',
	tag: 'dg-card-reveal',
})
export class DgCardReveal {
	@Prop() delay = 250;
	@Prop() staggerRange = 750;
	@State() activated = false;

	componentDidLoad() {
		setTimeout(
			() => this.activated = true,
			this.delay + this.staggerRange * Math.random(),
		);
	}

	render() {
		return (
			<div class={`card-reveal-container ${this.activated ? 'activated': ''}`}>
				<slot/>
			</div>
		);
	}
}
