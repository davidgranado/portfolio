import { Component, Prop, Event, EventEmitter } from '@stencil/core';
import classnames from 'classnames';

@Component({
	styleUrl: 'dg-modal.css',
	tag: 'dg-modal',
})
export class DgModal {
	@Prop() open = false;
	@Prop() heading = '';
	@Event() close: EventEmitter;

	handleClose() {
		this.close.emit();
	}

	render() {
		return (
			<div class={classnames('modal', {
				active: this.open,
			})}>
				<div
					onClick={() => this.handleClose()}
					class="modal-overlay"
				/>
				<div class="modal-container">
					<div class="modal-header">
						<button
							class="btn btn-clear float-right"
							onClick={() => this.handleClose()}
						></button>
						<div class="modal-title h4">
							{this.heading}
						</div>
					</div>
					<div class="modal-body">
						<slot name="content" />
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" onClick={() => this.handleClose()}>
							Close
						</button>
					</div>
				</div>
			</div>
		);
	}
}
