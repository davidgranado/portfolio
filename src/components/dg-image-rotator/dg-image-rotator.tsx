import { Component, Prop, State } from '@stencil/core';

@Component({
	styleUrl: 'dg-image-rotator.css',
	tag: 'dg-image-rotator',
})
export class DgImageRotator {
	@Prop() height: number;
	@Prop() width: number;
	@Prop() imageUrls: string[];
	@Prop() interval = 4000;
	@Prop() timingOffset = 0;

	@State() currentUrlIndex = 0;

	private timeoutRef: any = null;

	get nextImageIndex() {
		let nextIndex = this.currentUrlIndex + 1;

		if(nextIndex === this.imageUrls.length) {
			nextIndex = 0;
		}

		return nextIndex;
	}

	componentDidLoad() {
		setTimeout(() => this.setImageTimeout(), this.timingOffset);
	}

	componentDidUnload() {
		clearInterval(this.timeoutRef);
	}

	setImageTimeout() {
		if(this.timeoutRef) {
			clearTimeout(this.timeoutRef);
		}

		this.timeoutRef = setTimeout(() => this.triggerImageRotation(), this.interval);
	}

	triggerImageRotation() {
		this.rotateImage();
		this.setImageTimeout();
	}

	rotateImage() {
		this.currentUrlIndex = this.nextImageIndex;
	}

	render() {
		return (
			this.imageUrls.map((imageUrl, i) => (
				<img
					src={imageUrl}
					onClick={() => this.triggerImageRotation()}
					class={`img-responsive c-hand ${this.currentUrlIndex === i ? 'active' : ''}`}
				/>
			))
		)
	}
}
