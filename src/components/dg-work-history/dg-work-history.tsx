import { Component, Prop, State } from '@stencil/core';
import classnames from 'classnames';

import { WorkHistory } from '../../models';

@Component({
	tag: 'dg-work-history',
	styleUrl: 'dg-work-history.css',
})
export class DgWorkHistory {
	@Prop() history: WorkHistory[];
	@State() selectedItem = 0;

	render() {
		return (
			<div class="timeline">
				{this.history.map((historyItem, i) => (
					<div class="timeline-item">
						<div class="timeline-left">
							<button
								class={classnames('timeline-icon tooltip c-hand', {
									'icon-lg': this.selectedItem === i,
								})}
								onClick={(event) => this.selectItem(i, event.target as any)}
								data-tooltip={`${historyItem.startDate}-${historyItem.endDate}`}
							/>
						</div>
						<div class="timeline-content">
							<div class="tile">
								<div class="tile-content">
									{this.selectedItem !== i && (
										<p class="tile-title">
											{historyItem.title} - {historyItem.companyName}
										</p>
									)}
									{this.selectedItem === i && (
										<span>
											<p class="tile-title">
												{historyItem.title} - {historyItem.companyName}<br/>
												{historyItem.location}<br/>
												{historyItem.startDate} - {historyItem.endDate}
											</p>
											{historyItem.accomplishments && (
												<p class="tile-title">
													Responsibilities/Accomplishments
													<ul>
														{historyItem.accomplishments.map(a => (
															<li>
																{a}
															</li>
														))}
													</ul>
												</p>
											)}
											{historyItem.technologies && (
												<p class="tile-title">
													Technologies
													<ul>
														{historyItem.technologies.map(t => (
															<li>
																{t}
															</li>
														))}
													</ul>
												</p>
											)}
										</span>
									)}
								</div>
							</div>
						</div>
					</div>
				))}
			</div>
		);
	}

	private selectItem(itemIndex: number, button: HTMLButtonElement) {
		this.selectedItem = itemIndex;
		button.scrollIntoView({
			behavior: 'smooth',
			block: 'end',
		});
	}
}
