import { Component, State } from '@stencil/core';
import classnames from 'classnames';

@Component({
	tag: 'dg-app',
	styleUrl: 'dg-app.css',
})
export class DgApp {
	@State() selectedTabIndex = 0;
	@State() direction: 'left' | 'right' = 'left';
	@State() canNavigate = false;

	componentDidLoad() {
		// hack
		document.body.className = 'intro-running';
		setTimeout(() => {
			this.canNavigate = true;
			document.body.className = '';
		}, 6500);
	}

	render() {
		return (
			<div
				class={classnames({
					inactive: !this.canNavigate,
				})}
			>
				<dg-nav name="David Granado"></dg-nav>
				<div class="dg-app-container container">
					<span class="scroll-anchor"></span>
					<dg-page
						active={this.selectedTabIndex === 0}
						direction={this.direction}
					>
						<dg-projects-page/>
					</dg-page>
					<dg-page
						active={this.selectedTabIndex === 1}
						direction={this.direction}
					>
						<dg-work-history-page/>
					</dg-page>
					<dg-page
						active={this.selectedTabIndex === 2}
						direction={this.direction}
					>
						<dg-contact-page/>
					</dg-page>
				</div>
				<dg-tabs
					selectedTabIndex={this.selectedTabIndex}
					onSelectTab={({detail}) => this.updateSelectedTab(detail)}
					tabs={[
						'Projects',
						'Work History',
						'Contact',
					]}
				/>
			</div>
		);
	}

	private updateSelectedTab(tabIndex: number) {
		if(this.selectedTabIndex === tabIndex) {
			return;
		}

		this.direction = this.selectedTabIndex < tabIndex ? 'left' : 'right';
		this.selectedTabIndex = tabIndex;

		document.querySelector('.scroll-anchor').scrollIntoView({
			behavior: 'smooth',
			block: 'end',
		});
	}
}
