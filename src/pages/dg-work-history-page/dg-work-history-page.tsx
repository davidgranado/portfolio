import { Component } from '@stencil/core';
import { workHistory } from './work-history-data';

@Component({
	tag: 'dg-work-history-page',
	styleUrl: 'dg-work-history-page.css'
})
export class DgWorkHistoryPage {
	render() {
		return (
			<dg-work-history history={workHistory}/>
		);
	}
}
