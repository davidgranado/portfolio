import { Component } from '@stencil/core';

@Component({
	tag: 'dg-contact-page',
})
export class DgProjectPage {

	render() {
		return (
			<div>
				You can reach me at:
				<ul>
					<li>
						<a href="mailto:david@davidgranado.com">david@davidgranado.com</a>
					</li>
					<li>
						<a href="https://twitter.com/davidgranado" target="_blank">Twitter @davidgranado</a>
					</li>
				</ul>
			</div>
		);
	}
}
