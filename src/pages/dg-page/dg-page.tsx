import { Component, Prop } from '@stencil/core';
import classnames from 'classnames';

@Component({
	tag: 'dg-page',
	styleUrl: 'dg-page.css',
})
export class DgPage {
	@Prop() active: boolean;
	@Prop() direction: 'left' | 'right' = 'right';

	render() {
		return (
			<div class={classnames('dg-page', {
				'dg-page-active': this.active,
			})}>
				<slot/>
			</div>
		);
	}

	// @Watch('active')
	// activeHandler(isActive: boolean) {
	// 	if(isActive) {

	// 	}
	// }
}
