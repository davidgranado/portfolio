import { Project } from '../../models';

export
const projects: Project[] = [
	{
		content: '',
		title: 'ShoppeLister',
		subTitle: '03/2018',
		imageUrls: [
			'//davidgranado.com/images/portfolio/shoppelister-1.png',
			'//davidgranado.com/images/portfolio/shoppelister-2.png',
			'//davidgranado.com/images/portfolio/shoppelister-3.png',
		],
		labels: [
			'business',
			'active',
			'in-dev',
		],
		links: [
			{
				label: 'Site',
				url: 'https://shoppelister.com',
			},
		],
		technologies: [
			'Angular5',
			'Ionic',
			'Typescript',
			'Web Push Notifications',
			'Firebase Firestore',
			'Firebase Cloud Functions',
		],
		lessons: '',
	}, {
		content: '',
		title: 'QuickConference Apps',
		subTitle: '01/2018',
		imageUrls: [
			'//davidgranado.com/images/portfolio/conference-1.png',
			'//davidgranado.com/images/portfolio/conference-2.png',
			'//davidgranado.com/images/portfolio/conference-3.png',
		],
		labels: [
			'business',
			'hiatus',
		],
		links: [
			{
				label: 'Site',
				url: 'https://quickconferenceapps.com',
			},
		],
		technologies: [
			'Angular5',
			'Ionic',
			'Typescript',
		],
		lessons: ``,
	}, {
		content: '',
		title: 'Phungible',
		subTitle: '11/2016',
		imageUrls: [
			'//davidgranado.com/images/portfolio/phungible-1.png',
			'//davidgranado.com/images/portfolio/phungible-2.png',
			'//davidgranado.com/images/portfolio/phungible-3.png',
			'//davidgranado.com/images/portfolio/phungible-4.png',
		],
		labels: [
			'business',
			'hiatus',
		],
		links: [
			{
				label: 'Google Play Page',
				url: 'https://play.google.com/store/apps/details?id=com.phungible.tanado',
			}
		],
		technologies: [
			'React',
			'MobX',
			'Typescript',
			'MaterialUI',
			'PouchDB',
			'CouchDB',
			'Android',
			'Service Workers',
		],
		lessons: `
			For this project, I wanted to take a different approach
		`,
	}, {
		content: '',
		title: '"Space RTS" Game',
		subTitle: '04/2014',
		imageUrls: [
			'//davidgranado.com/images/portfolio/space-rts-1.png',
			'//davidgranado.com/images/portfolio/space-rts-2.png',
			'//davidgranado.com/images/portfolio/space-rts-3.png',
		],
		labels: [
			'hobby',
			'game',
		],
		links: [
			{
				label: 'Play (Todo)',
			}, {
				label: 'Gitlab Repo',
				url: 'https://gitlab.com/davidgranado/space-rts-game/',
			},
		],
		lessons: ``,
	}, {
		content: '',
		title: '"Meteor" Game',
		subTitle: '03/2014',
		imageUrls: [
			'//davidgranado.com/images/portfolio/meteor-game-1.png',
			'//davidgranado.com/images/portfolio/meteor-game-2.png',
		],
		labels: [
			'hobby',
			'game',
		],
		links: [
			{
				label: 'Play',
				url: 'https://davidgranado.com/projects/meteor-game/',
			}, {
				label: 'Gitlab Repo',
				url: 'https://gitlab.com/davidgranado/Meteor-Game/',
			},
		],
		lessons: ``,
	}, {
		content: '',
		title: 'Oncrex',
		subTitle: '02/2014',
		imageUrls: [
			'//davidgranado.com/images/portfolio/oncrex-1.png',
			'//davidgranado.com/images/portfolio/oncrex-2.png',
		],
		labels: [
			'business',
			'inactive',
		],
		links: [
			{
				label: 'Blog Entry (Todo)',
			},
		],
		technologies: [
			'KnockoutJS',
			'Node',
			'Gulp',
		],
	}, {
		content: '',
		title: 'Make a Set',
		subTitle: '11/2010',
		imageUrls: [
			'//davidgranado.com/images/portfolio/set-1.png',
			'//davidgranado.com/images/portfolio/set-2.png',
		],
		labels: [
			'hobby',
			'game',
		],
		links: [
			{
				label: 'Play',
				url: 'https://davidgranado.com/projects/2016-10-05/Make-a-Set/',
			}, {
				label: 'Blog Post',
				url: 'https://davidgranado.com/2016/10/06/set/',
			}, {
				label: 'Gitlab Repo',
				url: 'https://gitlab.com/davidgranado/Make-a-Set-Game/',
			},
		],
		lessons: ``,
	}, {
		content: 'Fooo!',
		title: 'Worm',
		subTitle: '9/2009',
		imageUrls: [
			'//davidgranado.com/images/portfolio/worm-1.png',
			'//davidgranado.com/images/portfolio/worm-2.png',
		],
		labels: [
			'hobby',
			'game',
		],
		links: [
			{
				label: 'Play',
				url: 'https://davidgranado.com/projects/2016-10-03/Worm/',
			}, {
				label: 'Blog Post',
				url: 'https://davidgranado.com/2016/10/04/worm/',
			}, {
				label: 'Gitlab Repo',
				url: 'https://gitlab.com/davidgranado/Worm/',
			},
		],
		lessons: ``,
	}, {
		content: 'Fooo!',
		title: 'Snake',
		subTitle: '9/2009',
		imageUrls: [
			'//davidgranado.com/images/portfolio/snake-1.png',
			'//davidgranado.com/images/portfolio/snake-2.png',
		],
		labels: [
			'hobby',
			'game',
		],
		links: [
			{
				label: 'Play',
				url: 'https://davidgranado.com/projects/2016-10-03/Snake/',
			}, {
				label: 'Blog Post',
				url: 'https://davidgranado.com/2016/10/03/snake/',
			}, {
				label: 'Gitlab Repo',
				url: 'https://gitlab.com/davidgranado/Snake',
			},
		],
		lessons: ``,
	},
];
