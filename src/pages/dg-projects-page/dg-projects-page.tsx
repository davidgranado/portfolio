import { Component } from '@stencil/core';

import { Project } from '../../models';
import { projects } from './project-data';

@Component({
	tag: 'dg-projects-page',
})
export class DgProjectsPage {
	private projects: Project[] = projects;

	render() {
		return (
			<dg-projects projects={this.projects} />
		);
	}
}
