export
interface WorkHistory {
	companyName: string;
	location: string;
	endDate: string;
	startDate: string;
	title: string;
	technologies?: string[];
	accomplishments?: string[];
}

export
type ProjectLabel = 'hiatus' |
'hobby' |
'business' |
'game' |
'in-dev' |
'inactive' |
'active';

interface ProjectLink {
	label: string;
	url?: string;
}


export
interface Project {
	lessons?: string;
	content: string;
	imageUrls: string[];
	links: ProjectLink[];
	labels: ProjectLabel[],
	technologies?: string[];
	subTitle: string;
	title: string;
}
